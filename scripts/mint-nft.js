require("dotenv").config();
const API_URL= process.env.API_URL;
const {createAlchemyWeb3}= require("@alch/alchemy-web3");
const web3 = createAlchemyWeb3(API_URL);
const PUBLIC_KEY= process.env.PUBLIC_KEY;
const PRIVATE_KEY= process.env.PRIVATE_KEY;
const contract = require("../artifacts/contracts/MyNFT.sol/MyNFT.json");
console.log(JSON.stringify(contract.abi));

const contractAddress = "0xd6dDa8f705E1Ab1f49B31b09Da86b687345148b4";
const nftContract = new web3.eth.Contract(contract.abi, contractAddress);
//Create Transaction
async function mintNFT(tokenURI){
    const nounce = await web3.eth.getTransactionCount(PUBLIC_KEY,"latest");
    const tx={
        'from' : PUBLIC_KEY,
        'to' : contractAddress,
        'gas' : 500000,
        'data' : nftContract.methods.mintNFT(PUBLIC_KEY,tokenURI).encodeABI(),
};

const signPromise = web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
  signPromise
    .then((signedTx) => {
      web3.eth.sendSignedTransaction(
        signedTx.rawTransaction,
        function (err, hash) {
          if (!err) {
            console.log(
              "The hash of your transaction is: ",
              hash,
              "\nCheck Alchemy's Mempool to view the status of your transaction!"
            );
          } else {
            console.log(
              "Something went wrong when submitting your transaction:",
              err
            );
          }
        }
      );
    })
    .catch((err) => {
      console.log(" Promise failed:", err);
    });
}
mintNFT(
  "https://gateway.pinata.cloud/ipfs/QmUtFDnKuBf6hEJc2wjS2KqZxBHJoz16m997vLJKH77K9M"
);