// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract MyNFT is ERC721URIStorage, Ownable{

    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    constructor() ERC721("MATADOR","MTD"){}
    
        // mint meas creation
        function mintNFT(address recipient, string memory tokenURI) public onlyOwner returns(uint){
           // uinque _tokenIds should be there we are incrementing
            _tokenIds.increment();
            // current value
            uint256 newItemId = _tokenIds.current();
            _mint(recipient,newItemId);
            _setTokenURI(newItemId,tokenURI);
            return newItemId;

        }
    
}